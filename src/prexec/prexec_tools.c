/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prexec_tools.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/04 17:31:09 by gcolomer          #+#    #+#             */
/*   Updated: 2022/03/04 17:31:09 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "prexec.h"
#include "../../libft/libft.h"
#include "../error/error.h"

#include <fcntl.h>

t_res	wrap_open(int *fd, char *pathname, int flags, mode_t mode)
{
	*fd = open(pathname, flags, mode);
	if (*fd == -1)
		return (error(NULL));
	return (SUCCESS);
}

t_res	arg_lst_to_split(t_lst *arg_lst, char ***m_arg)
{
	int	i;

	i = 0;
	*m_arg = malloc(sizeof(char *) * (lst_size(arg_lst) + 1));
	if (*m_arg == NULL)
		return (error(NULL));
	while (arg_lst != NULL)
	{
		(*m_arg)[i] = ft_strdup(arg_lst->content.arg);
		if ((*m_arg)[i] == NULL)
		{
			while (i > 0)
			{
				i -= 1;
				free((*m_arg)[i]);
			}
			return (error(NULL));
		}
		arg_lst = arg_lst->m_next;
		i++;
	}
	(*m_arg)[i] = NULL;
	return (SUCCESS);
}

t_res	var_lst_to_split(t_lst *var_lst, char ***m_var)
{
	int	i;

	i = 0;
	*m_var = malloc(sizeof(char *) * (lst_size(var_lst) + 1));
	if (*m_var == NULL)
		return (error(NULL));
	while (var_lst != NULL)
	{
		(*m_var)[i] = ft_strjoin_sep(var_lst->content.var.name,
				var_lst->content.var.value, '=');
		if ((*m_var)[i] == NULL)
		{
			while (i > 0)
			{
				i -= 1;
				free((*m_var)[i]);
			}
			free(*m_var);
			return (ERROR);
		}
		var_lst = var_lst->m_next;
		i++;
	}
	(*m_var)[i] = NULL;
	return (SUCCESS);
}

bool	is_echo_arg(char const *arg)
{
	int	i;

	i = 1;
	if (arg[0] != '-')
		return (false);
	while (arg[i])
	{
		if (arg[i] != 'n')
			return (false);
		i++;
	}
	return (true);
}
