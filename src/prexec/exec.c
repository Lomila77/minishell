/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/11 17:33:48 by gcolomer          #+#    #+#             */
/*   Updated: 2022/03/11 17:33:48 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "prexec.h"
#include "../../libft/libft.h"
#include "../error/error.h"

int	func_idx(char *cmd_name)
{
	const char	*builtin_names[] = {
		"echo",
		"cd",
		"pwd",
		"export",
		"unset",
		"env",
		"exit",
		NULL
	};
	int			i;

	i = 0;
	while (builtin_names[i] != NULL)
	{
		if (str_is_identical(cmd_name, (char *)builtin_names[i]) == YES)
			return (i);
		i++;
	}
	return (i);
}

typedef t_res	(*t_func)(t_cmd *cmd, t_lst **var_alst);

t_res	prexecution(t_cmd *cmd_ptr, t_lst **var_alst)
{
	const t_func	func[] = {f_echo, f_cd, f_pwd, f_export, f_unset,
		f_env, f_exit, f_exec};

	if (dup2(cmd_ptr->infile_fd, STDIN) == ERROR)
		return (error(NULL));
	if (dup2(cmd_ptr->outfile_fd, STDOUT) == ERROR)
		return (error(NULL));
	if (close(cmd_ptr->infile_fd) == ERROR)
		return (error(NULL));
	if (str_is_identical(cmd_ptr->m_args_lst->content.arg, "") == YES)
		return (error(NULL));
	if (func[func_idx(cmd_ptr->m_args_lst->content.arg)]
		(cmd_ptr, var_alst) == ERROR)
		return (ERROR);
	return (SUCCESS);
}

t_res	get_absolute_way_error(char *test_path)
{
	free(test_path);
	return (ERROR);
}

t_res	get_absolute_way(char *exec, char *path, char **cmd_path)
{
	char	*test_path;
	char	*piece_of_path;
	int		ret_access;

	test_path = NULL;
	ret_access = -1;
	while (ret_access != 0)
	{
		ft_free(&test_path);
		piece_of_path = ft_strcdup(path, ':');
		test_path = ft_strjoin_sep(piece_of_path, exec, '/');
		ft_free(&piece_of_path);
		if (test_path == NULL)
			return (error(NULL));
		ret_access = access(test_path, X_OK);
		while (*path != ':' && *path != '\0')
			path++;
		if (*path == '\0')
			break ;
		path++;
	}
	if (ret_access != 0)
		return (get_absolute_way_error(test_path));
	*cmd_path = test_path;
	return (SUCCESS);
}

t_res	f_exec(t_cmd *cmd, t_lst **var_alst)
{
	char	**m_arg;
	char	**m_var;
	char	*cmd_path;

	if (arg_lst_to_split(cmd->m_args_lst, &m_arg) == ERROR)
		return (ERROR);
	if (var_lst_to_split(*var_alst, &m_var) == ERROR)
	{
		free_matrice(m_arg);
		return (ERROR);
	}
	if (access(cmd->m_args_lst->content.arg, X_OK) == 0)
		cmd_path = m_arg[0];
	else if (get_absolute_way(m_arg[0], get_var(*var_alst, "PATH"),
			&cmd_path) == ERROR)
	{
		free_matrice(m_arg);
		free_matrice(m_var);
		return (ERROR);
	}
	free_minishell(&g_minishell);
	if (execve(cmd_path, m_arg, m_var) == ERROR)
		return (error(NULL));
	return (SUCCESS);
}
