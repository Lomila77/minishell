/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipe.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/09 14:55:31 by huthiess          #+#    #+#             */
/*   Updated: 2022/03/09 14:55:31 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../data.h"
#include "../../libft/libft.h"
#include "../error/error.h"

#include <unistd.h>

t_res	create_all_pipes(t_lst *cmd_lst)
{
	int		pipe_fd[2];
	t_lst	*prev_cmd;
	t_lst	*curr_cmd;

	curr_cmd = cmd_lst;
	curr_cmd->content.cmd.infile_fd = dup(STDIN);
	if (curr_cmd->content.cmd.infile_fd == -1)
		return (error(NULL));
	while (curr_cmd->m_next != NULL)
	{
		prev_cmd = curr_cmd;
		curr_cmd = curr_cmd->m_next;
		if (pipe(pipe_fd) != 0)
			return (error(NULL));
		prev_cmd->content.cmd.outfile_fd = pipe_fd[STDOUT];
		curr_cmd->content.cmd.infile_fd = pipe_fd[STDIN];
	}
	curr_cmd->content.cmd.outfile_fd = dup(STDOUT);
	if (curr_cmd->content.cmd.outfile_fd == -1)
		return (error(NULL));
	return (SUCCESS);
}
