/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fork.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/11 17:01:41 by gcolomer          #+#    #+#             */
/*   Updated: 2022/03/11 17:01:41 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "prexec.h"
#include "../error/error.h"
#include "../../libft/libft.h"

#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>

t_res	start_fork(t_lst *cmd_lst_ptr, t_lst **var_alst)
{
	t_res				child_exit_code;

	while (cmd_lst_ptr != NULL)
	{
		cmd_lst_ptr->content.cmd.pid = fork();
		if (cmd_lst_ptr->content.cmd.pid == ERROR)
			return (error(NULL));
		if (cmd_lst_ptr->content.cmd.pid == 0)
		{
			add_signal_handler(SIGINT, &exit_signal_handler);
			add_signal_handler(SIGQUIT, &exit_signal_handler);
			child_exit_code = prexecution(&cmd_lst_ptr->content.cmd, var_alst);
			free_minishell(&g_minishell);
			if (child_exit_code == ERROR)
			{
				error("minishell: command not found\n");
				exit(127);
			}
			else
				exit(EXIT_SUCCESS);
		}
		cmd_lst_ptr = cmd_lst_ptr->m_next;
	}
	return (SUCCESS);
}

void	close_all_pipes(t_lst *cmd_lst_ptr)
{
	while (cmd_lst_ptr != NULL)
	{
		close(cmd_lst_ptr->content.cmd.infile_fd);
		close(cmd_lst_ptr->content.cmd.outfile_fd);
		cmd_lst_ptr = cmd_lst_ptr->m_next;
	}
}

t_res	start_waitpid(t_lst *cmd_lst_ptr)
{
	int	return_status;

	close_all_pipes(cmd_lst_ptr);
	while (cmd_lst_ptr != NULL)
	{
		if (cmd_lst_ptr->content.cmd.pid > 0)
		{
			if (waitpid(cmd_lst_ptr->content.cmd.pid,
					&return_status, 0) == ERROR)
				return (error(NULL));
			if (WIFEXITED(return_status))
				g_minishell.last_return = WEXITSTATUS(return_status);
			else if (WIFSIGNALED(return_status))
				g_minishell.last_return = 128 + WTERMSIG(return_status);
			else
				g_minishell.last_return = -1;
		}
		cmd_lst_ptr = cmd_lst_ptr->m_next;
	}
	return (SUCCESS);
}

t_res	restore_fd(int target_fd, int backup_fd)
{
	close(target_fd);
	if (dup2(backup_fd, target_fd) == -1)
		return (ERROR);
	close(backup_fd);
	return (SUCCESS);
}

t_res	create_fork(t_lst **m_cmd_alst, t_lst **var_alst)
{
	int	std_fd[3];

	if (lst_size(*m_cmd_alst) == 1
		&& func_idx((*m_cmd_alst)->content.cmd.m_args_lst->content.arg) != 7)
	{
		std_fd[STDIN] = dup(STDIN);
		std_fd[STDOUT] = dup(STDOUT);
		std_fd[STDERR] = dup(STDERR);
		if (std_fd[STDIN] == -1 || std_fd[STDOUT] == -1 || std_fd[STDERR] == -1)
			return (ERROR);
		g_minishell.last_return = prexecution(&(*m_cmd_alst)->content.cmd,
				var_alst) == ERROR;
		if (restore_fd(STDIN, std_fd[STDIN]) == ERROR
			|| restore_fd(STDOUT, std_fd[STDOUT]) == ERROR
			|| restore_fd(STDERR, std_fd[STDERR]) == ERROR)
			return (ERROR);
	}
	else
	{
		if (start_fork(*m_cmd_alst, var_alst) == ERROR)
			return (ERROR);
		if (start_waitpid(*m_cmd_alst) == ERROR)
			return (ERROR);
	}
	return (SUCCESS);
}
