/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prexec.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/04 17:35:15 by gcolomer          #+#    #+#             */
/*   Updated: 2022/03/04 17:35:15 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PREXEC_H
# define PREXEC_H

# include "../data.h"

# include <fcntl.h>
# include <unistd.h>

t_res	wrap_open(int *fd, char *pathname, int flags, mode_t mode);
t_res	arg_lst_to_split(t_lst *arg_lst, char ***arg);
t_res	var_lst_to_split(t_lst *var_lst, char ***m_var);
bool	is_echo_arg(char const *arg);

t_res	heredoc(char *delim, int *fd);
t_res	append(char *file_name, int *fd);
t_res	redir_out(char *file_name, int *fd);
t_res	redir_in(char *file_name, int *fd);
t_res	create_all_redirs(t_lst *cmd_lst);
t_res	create_all_pipes(t_lst *cmd_lst);
t_res	create_all_forks(t_lst **cmd_alst);
int		func_idx(char *cmd_name);
t_res	prexecution(t_cmd *cmd_ptr, t_lst **var_alst);
t_res	create_fork(t_lst **m_cmd_alst, t_lst **var_alst);

t_res	f_echo(t_cmd *cmd_ptr, t_lst **var_alst);
t_res	f_cd(t_cmd *cmd_ptr, t_lst **var_alst);
t_res	f_pwd(t_cmd *cmd_ptr, t_lst **var_alst);
t_res	f_exit(t_cmd *cmd_ptr, t_lst **var_alst);

t_res	f_export(t_cmd *cmd_ptr, t_lst **var_alst);
t_res	f_unset(t_cmd *cmd_ptr, t_lst **var_alst);
t_res	f_env(t_cmd *cmd_ptr, t_lst **var_alst);

t_res	f_exec(t_cmd *cmd, t_lst **var_alst);

#endif
