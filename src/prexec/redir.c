/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redir.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/07 11:13:28 by gcolomer          #+#    #+#             */
/*   Updated: 2022/03/07 11:13:28 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "prexec.h"
#include "../../libft/libft.h"
#include "../error/error.h"

t_res	append(char *file_name, int *fd)
{
	if (close(*fd) == ERROR)
		return (error(NULL));
	if (wrap_open(fd, file_name, O_WRONLY | O_APPEND | O_CREAT,
			S_IWUSR | S_IRUSR) == ERROR)
		return (ERROR);
	return (SUCCESS);
}

t_res	redir_out(char *file_name, int *fd)
{
	if (close(*fd) == ERROR)
		return (error(NULL));
	if (wrap_open(fd, file_name, O_WRONLY | O_CREAT,
			S_IWUSR | S_IRUSR) == ERROR)
		return (ERROR);
	return (SUCCESS);
}

t_res	redir_in(char *file_name, int *fd)
{
	if (close(*fd) == ERROR)
		return (error(NULL));
	if (access(file_name, R_OK) == ERROR)
		return (error(NULL));
	if (wrap_open(fd, file_name, O_RDONLY, S_IRUSR) == ERROR)
		return (ERROR);
	return (SUCCESS);
}

t_res	create_redir(t_lst *redir, t_lst *cmd)
{
	if (redir->content.redir.type == REDIR_IN)
		if (redir_in(redir->content.redir.m_file_delim,
				&cmd->content.cmd.infile_fd) == ERROR)
			return (ERROR);
	if (redir->content.redir.type == REDIR_OUT)
		if (redir_out(redir->content.redir.m_file_delim,
				&cmd->content.cmd.outfile_fd) == ERROR)
			return (ERROR);
	if (redir->content.redir.type == REDIR_APPEND)
		if (append(redir->content.redir.m_file_delim,
				&cmd->content.cmd.outfile_fd) == ERROR)
			return (ERROR);
	if (redir->content.redir.type == REDIR_HEREDOC)
		if (heredoc(redir->content.redir.m_file_delim,
				&cmd->content.cmd.infile_fd) == ERROR)
			return (ERROR);
	return (SUCCESS);
}

t_res	create_all_redirs(t_lst *cmd_lst)
{
	t_lst	*curr_redir;

	while (cmd_lst != NULL)
	{
		curr_redir = cmd_lst->content.cmd.m_redir_lst;
		while (curr_redir != NULL)
		{
			if (create_redir(curr_redir, cmd_lst) == ERROR)
				return (ERROR);
			curr_redir = curr_redir->m_next;
		}
		cmd_lst = cmd_lst->m_next;
	}
	return (SUCCESS);
}
