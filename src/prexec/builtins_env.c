/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtins_env.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/22 16:48:26 by gcolomer          #+#    #+#             */
/*   Updated: 2022/03/22 16:48:26 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "prexec.h"
#include "../../libft/libft.h"
#include "../error/error.h"

#include <stdio.h>

t_res	f_export(t_cmd *cmd_ptr, t_lst **var_alst)
{
	t_lst	*var_ptr;
	t_lst	*arg_lst;

	var_ptr = *var_alst;
	arg_lst = cmd_ptr->m_args_lst->m_next;
	if (lst_size(arg_lst) >= 1)
	{
		while (arg_lst != NULL)
		{
			if (push_var(var_alst, arg_lst->content.arg, true) == ERROR)
				return (ERROR);
			arg_lst = arg_lst->m_next;
		}
	}
	else
	{
		while (var_ptr != NULL)
		{
			if (str_is_identical(var_ptr->content.var.name, "_") == NO)
				printf("export %s=\"%s\"\n", var_ptr->content.var.name,
					var_ptr->content.var.value);
			var_ptr = var_ptr->m_next;
		}
	}
	return (SUCCESS);
}

t_res	del_element(t_lst *arg_lst, t_lst *var_ptr, t_lst **var_alst,
					t_lst *previous_var_ptr)
{
	if (str_is_identical(arg_lst->content.arg,
			var_ptr->content.var.name) == YES)
	{
		if (var_ptr == *var_alst)
			*var_alst = var_ptr->m_next;
		else
			previous_var_ptr->m_next = var_ptr->m_next;
		lst_delone(&var_ptr);
		return (SUCCESS);
	}
	return (ERROR);
}

t_res	f_unset(t_cmd *cmd_ptr, t_lst **var_alst)
{
	t_lst	*var_ptr;
	t_lst	*previous_var_ptr;
	t_lst	*arg_lst;

	arg_lst = cmd_ptr->m_args_lst->m_next;
	if (arg_lst == NULL)
		return (SUCCESS);
	while (arg_lst != NULL)
	{
		var_ptr = *var_alst;
		while (var_ptr != NULL)
		{
			if (del_element(arg_lst, var_ptr, var_alst, previous_var_ptr)
				== SUCCESS)
				break ;
			previous_var_ptr = var_ptr;
			var_ptr = var_ptr->m_next;
		}
		arg_lst = arg_lst->m_next;
	}
	return (SUCCESS);
}

t_res	f_env(t_cmd *cmd_ptr, t_lst **var_alst)
{
	t_lst	*curr_var;

	if (cmd_ptr->m_args_lst->m_next != NULL)
		return (error(NULL));
	curr_var = *var_alst;
	while (curr_var != NULL)
	{
		printf("%s=%s\n", curr_var->content.var.name,
			curr_var->content.var.value);
		curr_var = curr_var->m_next;
	}
	return (SUCCESS);
}
