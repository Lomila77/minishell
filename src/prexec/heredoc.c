/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heredoc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/04 16:31:40 by gcolomer          #+#    #+#             */
/*   Updated: 2022/03/04 16:31:40 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "prexec.h"
#include "../../libft/libft.h"
#include "../error/error.h"

#include <stdio.h>
#include <readline/readline.h>
#include <sys/wait.h>

t_res	open_tmp(int *fd, int mode)
{
	if (wrap_open(fd, "/dev/shm/.minishell_tmp",
			mode | O_CREAT, S_IRUSR | S_IWUSR) == ERROR)
		return (ERROR);
	return (SUCCESS);
}

void	heredoc_readline(char *delim)
{
	char	*m_line;
	int		fd;
	int		exit_code;

	add_signal_handler(SIGINT, &exit_signal_handler);
	exit_code = 1;
	if (open_tmp(&fd, O_WRONLY) == ERROR)
		exit_code = -1;
	while (exit_code == 1)
	{
		m_line = readline("> ");
		if (m_line == NULL)
			exit_code = error(NULL);
		else if (str_is_identical(m_line, delim) == YES)
			exit_code = 0;
		else if (write(fd, m_line, ft_strlen(m_line)) == ERROR
			|| write(fd, "\n", 1) == ERROR)
			exit_code = error(NULL);
		ft_free(&m_line);
	}
	close(fd);
	free_minishell(&g_minishell);
	exit(exit_code);
}

t_res	heredoc(char *delim, int *fd)
{
	int	ret;
	int	pid;

	ret = 0;
	if (close(*fd) == ERROR)
		return (error(NULL));
	pid = fork();
	if (pid == 0)
		heredoc_readline(delim);
	else
	{
		waitpid(pid, &ret, 0);
		g_minishell.last_return = WEXITSTATUS(ret);
		if (g_minishell.last_return != 0)
		{
			write(1, "\n", 1);
			return (ERROR);
		}
		if (open_tmp(fd, O_RDONLY) == ERROR)
			return (ERROR);
	}
	unlink("/dev/shm/.minishell_tmp");
	return (SUCCESS);
}
