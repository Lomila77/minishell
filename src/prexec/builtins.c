/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtins.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/22 16:46:36 by gcolomer          #+#    #+#             */
/*   Updated: 2022/03/22 16:46:36 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "prexec.h"
#include "../../libft/libft.h"
#include "../error/error.h"

#include <errno.h>
#include <stdio.h>

t_res	f_echo(t_cmd *cmd_ptr, t_lst **var_alst)
{
	bool	dash_n;
	t_lst	*curr_arg;

	(void) var_alst;
	dash_n = false;
	curr_arg = cmd_ptr->m_args_lst->m_next;
	if (curr_arg != NULL)
	{
		if (is_echo_arg(curr_arg->content.arg))
		{
			dash_n = true;
			curr_arg = curr_arg->m_next;
		}
	}
	while (curr_arg != NULL)
	{
		printf("%s", curr_arg->content.arg);
		curr_arg = curr_arg->m_next;
		if (curr_arg != NULL)
			printf(" ");
	}
	if (dash_n == false)
		printf("\n");
	return (SUCCESS);
}

t_res	get_curr_dir(char **cwd_ptr)
{
	size_t	size;

	*cwd_ptr = NULL;
	size = 32;
	while (true)
	{
		size *= 2;
		free(*cwd_ptr);
		*cwd_ptr = malloc(sizeof(char) * size);
		if (*cwd_ptr == NULL)
			return (error(NULL));
		if (getcwd(*cwd_ptr, size) != NULL)
			break ;
		if (errno != ERANGE)
		{
			free(*cwd_ptr);
			return (error(NULL));
		}
	}
	return (SUCCESS);
}

t_res	f_cd(t_cmd *cmd_ptr, t_lst **var_alst)
{
	t_lst	*var_ptr;

	var_ptr = *var_alst;
	if (lst_size(cmd_ptr->m_args_lst) > 2)
		return (error("cd: Too many arguments\n"));
	if (lst_size(cmd_ptr->m_args_lst) < 2)
		return (error("cd: Too few arguments\n"));
	if (chdir(cmd_ptr->m_args_lst->m_next->content.arg) == ERROR)
		return (error("cd: No such file or directory\n"));
	while (var_ptr != NULL)
	{
		if (str_is_identical(var_ptr->content.var.name, "PWD") == YES)
		{
			free(var_ptr->content.var.value);
			if (get_curr_dir(&var_ptr->content.var.value) == ERROR)
				return (ERROR);
			return (SUCCESS);
		}
		var_ptr = var_ptr->m_next;
	}
	return (error(NULL));
}

t_res	f_pwd(t_cmd *cmd_ptr, t_lst **var_alst)
{
	char	*cwd;

	(void)var_alst;
	if (lst_size(cmd_ptr->m_args_lst) != 1)
		return (ERROR);
	if (get_curr_dir(&cwd) == ERROR)
		return (ERROR);
	printf("%s\n", cwd);
	free(cwd);
	return (SUCCESS);
}

t_res	f_exit(t_cmd *cmd_ptr, t_lst **var_alst)
{
	int	exit_code;

	(void)cmd_ptr;
	(void)var_alst;
	if (cmd_ptr->m_args_lst->m_next == NULL)
		exit_code = 0;
	else
	{
		if (str_is_num(cmd_ptr->m_args_lst->m_next->content.arg) == NO)
			return (error("exit: Numeric arguments required\n"));
		ft_atoi(cmd_ptr->m_args_lst->m_next->content.arg, &exit_code);
	}
	free_minishell(&g_minishell);
	exit(exit_code);
	return (SUCCESS);
}
