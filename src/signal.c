/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signal.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/08 21:40:24 by huthiess          #+#    #+#             */
/*   Updated: 2022/06/08 21:40:24 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "data.h"

#include <signal.h>
#include <stdlib.h>

void	add_signal_handler(int signal, void (*handler)(int))
{
	struct sigaction	sig_action;

	sig_action.sa_flags = 0;
	sig_action.sa_handler = handler;
	sigemptyset(&sig_action.sa_mask);
	sigaction(signal, &sig_action, NULL);
}

void	exit_signal_handler(int signal)
{
	free_minishell(&g_minishell);
	exit(128 + signal);
}
