/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/20 10:36:44 by huthiess          #+#    #+#             */
/*   Updated: 2022/04/20 10:36:44 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "data.h"
#include "../libft/libft.h"

#include <readline/readline.h>

void	free_minishell(t_data *data)
{
	lst_clear(&data->m_tk_alst);
	lst_clear(&data->m_cmd_alst);
	lst_clear(&data->m_var_alst);
	ft_free(&data->m_cmdline);
	rl_clear_history();
}
