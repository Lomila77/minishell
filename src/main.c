/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/27 11:37:54 by gcolomer          #+#    #+#             */
/*   Updated: 2022/02/27 11:37:54 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "data.h"
#include "var/var.h"
#include "token/tokenizer.h"
#include "../libft/libft.h"
#include "prexec/prexec.h"

#include <signal.h>
#include <unistd.h>
#include <readline/history.h>
#include <readline/readline.h>

t_data	g_minishell;

t_res	ret_free_cmdline(t_res res, char **m_cmdline, t_lst **m_tk_alst,
			t_lst **m_cmd_alst)
{
	ft_free(m_cmdline);
	lst_clear(m_tk_alst);
	lst_clear(m_cmd_alst);
	return (res);
}

t_res	interpret_cmdline(char **m_cmdline, t_lst **m_tk_alst,
			t_lst **m_cmd_alst, t_lst **m_var_alst)
{
	if (**m_cmdline != '\0')
		add_history(*m_cmdline);
	tokenizer(*m_cmdline, m_tk_alst);
	parse(*m_tk_alst, m_cmd_alst, *m_var_alst);
	if (*m_cmd_alst != NULL)
	{
		if (create_all_pipes(*m_cmd_alst) == ERROR)
			return (ret_free_cmdline(SUCCESS, m_cmdline, m_tk_alst,
					m_cmd_alst));
		if (create_all_redirs(*m_cmd_alst) == ERROR)
			return (ret_free_cmdline(SUCCESS, m_cmdline, m_tk_alst,
					m_cmd_alst));
		if (create_fork(m_cmd_alst, m_var_alst) == ERROR)
			return (ret_free_cmdline(SUCCESS, m_cmdline, m_tk_alst,
					m_cmd_alst));
	}
	return (ret_free_cmdline(SUCCESS, m_cmdline, m_tk_alst, m_cmd_alst));
}

void	readline_signal_handler(int signal)
{
	(void)signal;
	replace_line();
	g_minishell.last_return = 128 + signal;
}

int	main(int argc, char *argv[], char *envp[])
{
	(void)argc;
	(void)argv;
	g_minishell.m_tk_alst = NULL;
	g_minishell.m_cmd_alst = NULL;
	g_minishell.m_cmdline = NULL;
	if (isatty(STDIN))
	{
		add_signal_handler(SIGINT, SIG_IGN);
		add_signal_handler(SIGQUIT, SIG_IGN);
	}
	if (init_var(&g_minishell.m_var_alst, envp) == ERROR)
		return (EXIT_FAILURE);
	while (CONTINUE)
	{
		add_signal_handler(SIGINT, &readline_signal_handler);
		g_minishell.m_cmdline = readline("minishell $ ");
		if (g_minishell.m_cmdline == NULL)
			break ;
		add_signal_handler(SIGINT, SIG_IGN);
		interpret_cmdline(&g_minishell.m_cmdline, &g_minishell.m_tk_alst,
			&g_minishell.m_cmd_alst, &g_minishell.m_var_alst);
	}
	free_minishell(&g_minishell);
	return (EXIT_SUCCESS);
}
