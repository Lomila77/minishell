/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools_token.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/28 16:47:17 by gcolomer          #+#    #+#             */
/*   Updated: 2022/02/28 16:47:17 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tokenizer.h"
#include "../../libft/libft.h"
#include "../error/error.h"

int	sl_quote(char *pipeline, char quote)
{
	int	i;

	i = 0;
	while (pipeline[i] != quote)
	{
		if (!pipeline[i])
			return (-1);
		i++;
	}
	return (i);
}

int	is_whitespace(char c)
{
	if (c == '\t' || c == ' ')
		return (YES);
	return (NO);
}

int	is_redir(char c)
{
	if (c == '<' || c == '>')
		return (YES);
	return (NO);
}

int	char_is_special(char *pipeline, char c)
{
	if (c == '\'' || c == '\"')
	{
		if (sl_quote(pipeline, '\'') == -1 && c == '\'')
			return (error("minishell: Bad format\n"));
		else if (sl_quote(pipeline, '\"') == -1 && c == '\"')
			return (error("minishell: Bad format\n"));
		else
			return (YES);
	}
	else if (is_redir(c) == YES || c == '|'
		|| is_whitespace(c) == YES)
		return (YES);
	return (NO);
}
