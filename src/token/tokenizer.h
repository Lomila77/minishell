/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tokenizer.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/28 14:29:53 by gcolomer          #+#    #+#             */
/*   Updated: 2022/02/28 14:29:53 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TOKENIZER_H
# define TOKENIZER_H

# include "../data.h"

void	tokenizer(char *pipeline, t_lst **alst);
int		get_str(char **pipeline_ptr, t_lst **str_lst);
int		get_quoted_str(char **pipeline_ptr, t_lst **str_lst);
int		get_redir(char **pipeline, t_lst **alst);

int		tk_string(char **pipeline, t_lst **alst);
int		tk_quoted_string(char **pipeline, t_lst **alst);
int		tk_pipe(char **pipeline, t_lst **alst);

int		sl_quote(char *pipeline, char quote);
int		char_is_special(char *pipeline, char c);
int		is_whitespace(char c);
int		is_redir(char c);

#endif
