/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tokenizer.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/28 14:27:50 by gcolomer          #+#    #+#             */
/*   Updated: 2022/02/28 14:27:50 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tokenizer.h"
#include "../../libft/libft.h"
#include "../error/error.h"

int	get_redir(char **pipeline, t_lst **alst)
{
	if (**pipeline == '\0')
		return (error(NULL));
	if ((*pipeline)[0] == '<' && (*pipeline)[1] == '<')
	{
		*pipeline += 1;
		if (push_tk(alst, CHAR_REDIR_HEREDOC) == ERROR)
			return (ERROR);
	}
	else if ((*pipeline)[0] == '>' && (*pipeline)[1] == '>')
	{
		*pipeline += 1;
		if (push_tk(alst, CHAR_REDIR_APPEND) == ERROR)
			return (ERROR);
	}
	else if ((*pipeline)[0] == '<' && push_tk(alst, CHAR_REDIR_IN) == ERROR)
		return (lst_clear(alst));
	else if ((*pipeline)[0] == '>' && push_tk(alst, CHAR_REDIR_OUT) == ERROR)
		return (lst_clear(alst));
	*pipeline += 1;
	return (SUCCESS);
}

int	get_str(char **pipeline_ptr, t_lst **str_lst)
{
	int		i;
	int		ret;
	char	*m_tk;

	i = 0;
	ret = NO;
	while (ret != YES && (*pipeline_ptr)[i])
	{
		ret = char_is_special(*pipeline_ptr + i, (*pipeline_ptr)[i]);
		if (ret == ERROR)
			return (ERROR);
		else if (ret == NO)
			i++;
	}
	m_tk = ft_substr(*pipeline_ptr, 0, i);
	if (m_tk == NULL)
		return (error(NULL));
	if (push_str(*str_lst, NON_QUOTED_STR, m_tk) == ERROR)
		return (ft_free(&m_tk));
	*pipeline_ptr += i;
	if (**pipeline_ptr == '\'' || **pipeline_ptr == '\"')
		if (get_quoted_str(pipeline_ptr, str_lst) == ERROR)
			return (lst_clear(str_lst));
	return (SUCCESS);
}

int	get_quoted_str(char **pipeline_ptr, t_lst **str_lst)
{
	int		i;
	char	*m_tk;

	i = sl_quote(*pipeline_ptr + 1, **pipeline_ptr);
	*pipeline_ptr += 1;
	if (i == -1)
		return (error(NULL));
	m_tk = ft_substr(*pipeline_ptr, 0, i);
	if (m_tk == NULL)
		return (error(NULL));
	if ((*pipeline_ptr)[i] == '\''
		&& push_str(*str_lst, SMP_QUOTED_STR, m_tk) == ERROR)
		return (ft_free(&m_tk));
	else if ((*pipeline_ptr)[i] == '\"'
		&& push_str(*str_lst, DBL_QUOTED_STR, m_tk) == ERROR)
		return (ft_free(&m_tk));
	*pipeline_ptr += i + 1;
	if (**pipeline_ptr == '\'' || **pipeline_ptr == '\"')
		if (get_quoted_str(pipeline_ptr, str_lst) == ERROR)
			return (lst_clear(str_lst));
	if (**pipeline_ptr != '\0'
		&& char_is_special(*pipeline_ptr, **pipeline_ptr) == NO)
		if (get_str(pipeline_ptr, str_lst) == ERROR)
			return (lst_clear(str_lst));
	return (SUCCESS);
}

int	tk_str(char **pipeline, t_lst **alst)
{
	if (char_is_special(*pipeline, **pipeline) == NO)
	{
		if (tk_string(pipeline, alst) == ERROR)
			return (ERROR);
	}
	else if ((**pipeline == '\'' || **pipeline == '\"')
		&& sl_quote(*pipeline + 1, **pipeline) != -1)
	{
		if (tk_quoted_string(pipeline, alst) == ERROR)
			return (ERROR);
	}
	else
		return (ERROR);
	return (SUCCESS);
}

void	tokenizer(char *line, t_lst **alst)
{
	bool	valid;

	while (*line)
	{
		valid = false;
		if (is_whitespace(*line) == YES)
		{
			line++;
			valid = true;
		}
		else if (((*line == '\'' || *line == '\"')
				&& char_is_special(line, *line) == YES)
			|| (!(*line == '\'' || *line == '\"')
				&& char_is_special(line, *line) == NO))
			valid = tk_str(&line, alst) != ERROR;
		else if (is_redir(*line) == YES)
			valid = get_redir(&line, alst) != ERROR;
		else if (*line == '|')
			valid = tk_pipe(&line, alst) != ERROR;
		if (valid == false)
		{
			lst_clear(alst);
			return ;
		}
	}
}
