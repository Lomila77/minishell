/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tokenizer_bis.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/04 12:52:15 by gcolomer          #+#    #+#             */
/*   Updated: 2022/03/04 12:52:15 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tokenizer.h"
#include "../../libft/libft.h"

int	tk_string(char **pipeline, t_lst **alst)
{
	if (push_str_lst(alst) == ERROR)
		return (ERROR);
	if (get_str(pipeline, lst_ultimate(alst)) == ERROR)
		return (ERROR);
	return (SUCCESS);
}

int	tk_quoted_string(char **pipeline, t_lst **alst)
{
	if (push_str_lst(alst) == ERROR)
		return (ERROR);
	if (get_quoted_str(pipeline, lst_ultimate(alst)) == ERROR)
		return (ERROR);
	return (SUCCESS);
}

int	tk_pipe(char **pipeline, t_lst **alst)
{
	if (push_tk(alst, CHAR_PIPE) == ERROR)
		return (ERROR);
	*pipeline += 1;
	return (SUCCESS);
}
