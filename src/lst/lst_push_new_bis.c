/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_push_new_bis.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/04 09:48:55 by gcolomer          #+#    #+#             */
/*   Updated: 2022/03/04 09:48:55 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../data.h"
#include "../../libft/libft.h"

t_res	push_tk(t_lst **tk_alst, t_tk_type type)
{
	t_lst			*new;

	if (new_tk(&new, type) == ERROR)
		return (ERROR);
	lst_add_back(tk_alst, new);
	return (SUCCESS);
}

t_res	push_str_lst(t_lst **tk_alst)
{
	t_lst			*new;

	if (new_str_lst(&new) == ERROR)
		return (ERROR);
	lst_add_back(tk_alst, new);
	return (SUCCESS);
}

t_res	push_str(t_lst *str_lst, t_tk_type type, char *str)
{
	t_lst			*new;

	if (new_str(&new, type, str) == ERROR)
		return (ERROR);
	lst_add_back(&str_lst->content.tk.content.m_str_lst, new);
	return (SUCCESS);
}
