/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_new.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/28 15:32:57 by huthiess          #+#    #+#             */
/*   Updated: 2022/02/28 15:32:57 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../data.h"
#include "../../libft/libft.h"
#include "../error/error.h"

#include <stdlib.h>

t_res	lst_new(t_lst **alst)
{
	*alst = malloc(sizeof(t_lst));
	if (*alst == NULL)
		return (error(NULL));
	(*alst)->m_next = NULL;
	return (SUCCESS);
}

t_res	new_redir(t_lst **redir_alst, t_redir_type type,
		char *m_file_delim)
{
	if (lst_new(redir_alst) == ERROR)
		return (ERROR);
	(*redir_alst)->type = REDIR;
	(*redir_alst)->content.redir.type = type;
	(*redir_alst)->content.redir.m_file_delim = m_file_delim;
	return (SUCCESS);
}

t_res	new_arg(t_lst **arg_alst, char *arg_str)
{
	if (lst_new(arg_alst) == ERROR)
		return (ERROR);
	(*arg_alst)->type = ARG;
	(*arg_alst)->content.arg = arg_str;
	return (SUCCESS);
}

/* TODO Maybe add nb_args, etc. to function args */
t_res	new_cmd(t_lst **cmd_alst)
{
	if (lst_new(cmd_alst) == ERROR)
		return (ERROR);
	(*cmd_alst)->type = CMD;
	(*cmd_alst)->content.cmd.m_args_lst = NULL;
	(*cmd_alst)->content.cmd.m_redir_lst = NULL;
	(*cmd_alst)->content.cmd.infile_fd = -1;
	(*cmd_alst)->content.cmd.outfile_fd = -1;
	(*cmd_alst)->content.cmd.pid = -1;
	(*cmd_alst)->content.cmd.append = false;
	return (SUCCESS);
}
