/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_last.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/28 17:13:00 by huthiess          #+#    #+#             */
/*   Updated: 2022/02/28 17:13:00 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../data.h"

#include <stddef.h>

t_lst	*lst_last(t_lst *lst)
{
	if (lst != NULL)
	{
		while (lst->m_next != NULL)
			lst = lst->m_next;
	}
	return (lst);
}

t_lst	**lst_ultimate(t_lst **lst)
{
	t_lst	**current;

	current = lst;
	if (*current != NULL)
	{
		while ((*current)->m_next != NULL)
			current = &(*current)->m_next;
	}
	return (current);
}
