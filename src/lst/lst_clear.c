/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_clear.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/28 17:00:35 by huthiess          #+#    #+#             */
/*   Updated: 2022/02/28 17:00:35 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../data.h"
#include "../../libft/libft.h"

t_res	lst_clear(t_lst **lst)
{
	t_lst	*next;

	while (*lst != NULL)
	{
		next = (*lst)->m_next;
		lst_delone(lst);
		*lst = next;
	}
	return (ERROR);
}
