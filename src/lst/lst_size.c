/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_size.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/08 13:58:58 by gcolomer          #+#    #+#             */
/*   Updated: 2022/03/08 13:58:58 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../data.h"

int	lst_size(t_lst *lst)
{
	if (lst == NULL)
		return (0);
	if (lst->m_next != NULL)
		return (lst_size(lst->m_next) + 1);
	return (1);
}
