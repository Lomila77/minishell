/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_delone.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/28 16:45:52 by huthiess          #+#    #+#             */
/*   Updated: 2022/02/28 16:45:52 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../data.h"
#include "../../libft/libft.h"

static void	close_fd(int fd)
{
	if (fd != -1)
		close(fd);
}

/* REVIEW: Need to test lst == NULL? */
void	lst_delone(t_lst **lst)
{
	if ((*lst)->type == TK && (*lst)->content.tk.type == STRING)
		lst_clear(&(*lst)->content.tk.content.m_str_lst);
	else if ((*lst)->type == TK
		&& ((*lst)->content.tk.type == DBL_QUOTED_STR
			|| (*lst)->content.tk.type == SMP_QUOTED_STR
			|| (*lst)->content.tk.type == NON_QUOTED_STR))
		ft_free(&(*lst)->content.tk.content.m_str);
	else if ((*lst)->type == REDIR)
		ft_free(&(*lst)->content.redir.m_file_delim);
	else if ((*lst)->type == ARG)
		ft_free(&(*lst)->content.arg);
	else if ((*lst)->type == CMD)
	{
		lst_clear(&(*lst)->content.cmd.m_args_lst);
		lst_clear(&(*lst)->content.cmd.m_redir_lst);
		close_fd((*lst)->content.cmd.infile_fd);
		close_fd((*lst)->content.cmd.outfile_fd);
	}
	else if ((*lst)->type == VAR)
	{
		ft_free(&(*lst)->content.var.name);
		ft_free(&(*lst)->content.var.value);
	}
	ft_free((char **)lst);
}
