/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_add_front.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/08 14:16:22 by huthiess          #+#    #+#             */
/*   Updated: 2022/03/08 14:16:22 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../data.h"

#include <stddef.h>

void	lst_add_front(t_lst **alst, t_lst *new)
{
	if (*alst != NULL)
		new->m_next = *alst;
	*alst = new;
}
