/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_push_new.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/28 17:55:29 by huthiess          #+#    #+#             */
/*   Updated: 2022/02/28 17:55:29 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../data.h"
#include "../../libft/libft.h"

t_res	push_redir(t_lst **redir_alst, t_redir_type type,
			char *m_file_delim)
{
	t_lst	*new;

	if (new_redir(&new, type, m_file_delim) == ERROR)
		return (ERROR);
	lst_add_back(redir_alst, new);
	return (SUCCESS);
}

t_res	push_arg(t_lst **arg_alst, char *arg_str)
{
	t_lst	*new;

	if (new_arg(&new, arg_str) == ERROR)
		return (ERROR);
	lst_add_back(arg_alst, new);
	return (SUCCESS);
}

t_res	push_cmd(t_lst **cmd_alst)
{
	t_lst	*new;

	if (new_cmd(&new) == ERROR)
		return (ERROR);
	lst_add_back(cmd_alst, new);
	return (SUCCESS);
}
