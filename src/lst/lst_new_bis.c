/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_new_bis.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/04 09:51:49 by gcolomer          #+#    #+#             */
/*   Updated: 2022/03/04 09:51:49 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../data.h"
#include "../../libft/libft.h"

t_res	new_tk(t_lst **tk_alst, t_tk_type type)
{
	if (lst_new(tk_alst) == ERROR)
		return (ERROR);
	(*tk_alst)->type = TK;
	(*tk_alst)->content.tk.type = type;
	(*tk_alst)->content.tk.content.m_str = NULL;
	return (SUCCESS);
}

t_res	new_str_lst(t_lst **tk_alst)
{
	if (lst_new(tk_alst) == ERROR)
		return (ERROR);
	(*tk_alst)->type = TK;
	(*tk_alst)->content.tk.type = STRING;
	(*tk_alst)->content.tk.content.m_str_lst = NULL;
	return (SUCCESS);
}

t_res	new_str(t_lst **tk_alst, t_tk_type type, char *str)
{
	if (lst_new(tk_alst) == ERROR)
		return (ERROR);
	(*tk_alst)->type = TK;
	(*tk_alst)->content.tk.type = type;
	(*tk_alst)->content.tk.content.m_str = str;
	return (SUCCESS);
}
