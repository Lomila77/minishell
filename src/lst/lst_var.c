/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_var.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/04 10:12:08 by huthiess          #+#    #+#             */
/*   Updated: 2022/03/04 10:12:08 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../data.h"
#include "../var/var.h"
#include "../../libft/libft.h"

#include <stdbool.h>

t_res	new_var(t_lst **var_alst, char *assign, bool is_exported)
{
	if (lst_new(var_alst) == ERROR)
		return (ERROR);
	(*var_alst)->type = VAR;
	(*var_alst)->content.var.is_exported = is_exported;
	(*var_alst)->content.var.name = NULL;
	(*var_alst)->content.var.value = NULL;
	if (parse_var_assign(assign, &(*var_alst)->content.var.name,
			&(*var_alst)->content.var.value) == ERROR)
	{
		lst_delone(var_alst);
		return (ERROR);
	}
	return (SUCCESS);
}

t_lst	*get_var_ptr(t_lst *var_alst, t_lst **prec_old, char *name)
{
	*prec_old = NULL;
	while (var_alst != NULL)
	{
		if (str_is_identical(name, var_alst->content.var.name) == YES)
			return (var_alst);
		*prec_old = var_alst;
		var_alst = var_alst->m_next;
	}
	return (NULL);
}

t_res	push_var(t_lst **var_alst, char *assign, bool is_exported)
{
	t_lst	*new;
	t_lst	*prec_old;
	t_lst	*old;

	if (new_var(&new, assign, is_exported) == ERROR)
		return (ERROR);
	old = get_var_ptr(*var_alst, &prec_old, new->content.var.name);
	if (old != NULL)
	{
		if (prec_old != NULL)
			prec_old->m_next = old->m_next;
		else
			*var_alst = old->m_next;
		lst_delone(&old);
	}
	lst_add_front(var_alst, new);
	return (SUCCESS);
}

char	*get_var(t_lst *var_alst, char *name)
{
	t_lst	*var;
	t_lst	*unused;

	var = get_var_ptr(var_alst, &unused, name);
	if (var != NULL)
		return (var->content.var.value);
	else
		return ("");
}
