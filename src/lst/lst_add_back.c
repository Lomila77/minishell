/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_add_back.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/28 17:14:26 by huthiess          #+#    #+#             */
/*   Updated: 2022/02/28 17:14:26 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../data.h"
#include "../../libft/libft.h"

void	lst_add_back(t_lst **alst, t_lst *new)
{
	t_lst	*last;

	last = lst_last(*alst);
	if (last == NULL)
		*alst = new;
	else
		last->m_next = new;
}
