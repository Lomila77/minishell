/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/08 14:46:18 by gcolomer          #+#    #+#             */
/*   Updated: 2022/03/08 14:46:18 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "error.h"
#include "../data.h"
#include "../../libft/libft.h"

#include <stdio.h>
#include <errno.h>
#include <string.h>

int	error(const char *msg)
{
	if (msg != NULL)
		ft_putstr_fd(msg, STDERR);
	else if (errno == 0)
		ft_putstr_fd("minishell: Error\n", STDERR);
	else
		perror(strerror(errno));
	g_minishell.last_return = EXIT_FAILURE;
	return (ERROR);
}
