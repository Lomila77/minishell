/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   data.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/27 11:38:50 by gcolomer          #+#    #+#             */
/*   Updated: 2022/02/27 11:38:50 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DATA_H
# define DATA_H

# include <stdbool.h>
# include <unistd.h>

typedef int				t_res;

typedef struct s_lst	t_lst;

typedef enum e_tk_type
{
	STRING,
	DBL_QUOTED_STR,
	SMP_QUOTED_STR,
	NON_QUOTED_STR,
	CHAR_PIPE,
	CHAR_REDIR_IN = 10,
	CHAR_REDIR_OUT = 11,
	CHAR_REDIR_APPEND = 12,
	CHAR_REDIR_HEREDOC = 13
}	t_tk_type;

typedef union u_tk_content
{
	char	*m_str;
	t_lst	*m_str_lst;
}	t_tk_content;

typedef struct s_tk
{
	t_tk_type		type;
	t_tk_content	content;
}	t_tk;

typedef enum e_redir_type
{
	REDIR_IN = 10,
	REDIR_OUT = 11,
	REDIR_APPEND = 12,
	REDIR_HEREDOC = 13
}	t_redir_type;

typedef struct s_redir
{
	t_redir_type	type;
	char			*m_file_delim;
}	t_redir;

typedef struct s_cmd
{
	t_lst	*m_args_lst;
	t_lst	*m_redir_lst;
	int		infile_fd;
	int		outfile_fd;
	pid_t	pid;
	bool	append;
}	t_cmd;

typedef struct s_var
{
	char	*name;
	char	*value;
	bool	is_exported;
}	t_var;

typedef enum e_type
{
	TK,
	REDIR,
	ARG,
	CMD,
	VAR
}	t_type;

typedef union u_content
{
	t_tk	tk;
	t_redir	redir;
	char	*arg;
	t_cmd	cmd;
	t_var	var;
}	t_content;

typedef struct s_lst
{
	t_type		type;
	t_content	content;
	t_lst		*m_next;
}	t_lst;

typedef struct s_data
{
	char	*m_cmdline;
	t_lst	*m_tk_alst;
	t_lst	*m_cmd_alst;
	t_lst	*m_var_alst;
	int		last_return;
}	t_data;

extern t_data			g_minishell;

void	exit_signal_handler(int signal);

t_res	new_tk(t_lst **tk_alst, t_tk_type type);
t_res	push_tk(t_lst **tk_alst, t_tk_type type);

t_res	new_str_lst(t_lst **tk_alst);
t_res	push_str_lst(t_lst **tk_alst);

t_res	new_str(t_lst **tk_alst, t_tk_type type, char *str);
t_res	push_str(t_lst *str_lst, t_tk_type type, char *str);

t_res	new_redir(t_lst **redir_alst, t_redir_type type,
			char *m_file_delim);
t_res	push_redir(t_lst **redir_alst, t_redir_type type,
			char *m_file_delim);

t_res	new_arg(t_lst **arg_alst, char *arg_str);
t_res	push_arg(t_lst **arg_alst, char *arg_str);

t_res	new_cmd(t_lst **cmd_alst);
t_res	push_cmd(t_lst **cmd_alst);

t_res	new_var(t_lst **var_alst, char *assign, bool is_exported);
t_res	push_var(t_lst **var_alst, char *assign, bool is_exported);
char	*get_var(t_lst *var_alst, char *name);

t_res	lst_new(t_lst **alst);
void	lst_delone(t_lst **lst);
t_res	lst_clear(t_lst **lst);
void	lst_add_back(t_lst **alst, t_lst *new);
void	lst_add_front(t_lst **alst, t_lst *new);
t_lst	*lst_last(t_lst *lst);
int		lst_size(t_lst *lst);
t_lst	**lst_ultimate(t_lst **lst);

void	parse(t_lst *tk_lst, t_lst **cmd_alst, t_lst *var_lst);

void	free_minishell(t_data *data);

void	add_signal_handler(int signal, void (*sa_handler)(int));
void	exit_signal_handler(int signal);
void	replace_line(void);

#endif
