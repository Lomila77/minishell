/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_var.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/04 10:16:14 by huthiess          #+#    #+#             */
/*   Updated: 2022/03/04 10:16:14 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../data.h"
#include "../../libft/libft.h"

#include <stdbool.h>

char	*ft_strndup(const char *str, size_t n)
{
	size_t	size;
	char	*new;
	void	*end;

	end = ft_memchr(str, '\0', n);
	if (end == NULL)
		size = n;
	else
		size = end - (void *)str;
	new = (char *)malloc(sizeof(*str) * (size + 1));
	if (new == NULL)
		return (NULL);
	return (ft_strncpy(new, str, size + 1));
}

bool	is_valid_var_name(char *str, int *end, bool test_equal)
{
	*end = 0;
	if (!(ft_isalpha(str[*end]) == YES || str[*end] == '_'))
		return (false);
	*end += 1;
	while (ft_isalnum(str[*end]) == YES || str[*end] == '_')
		*end += 1;
	if (test_equal)
		return (str[*end] == '=');
	else
		return (true);
}

t_res	parse_var_name(char *str, int *end, char **name_ptr, bool test_equal)
{
	if (is_valid_var_name(str, end, test_equal) == false)
		return (ERROR);
	*name_ptr = ft_strndup(str, *end);
	if (*name_ptr == NULL)
		return (ERROR);
	return (SUCCESS);
}

t_res	parse_var_assign(char *assign, char **name_ptr, char **value_ptr)
{
	int	eq_idx;

	if (parse_var_name(assign, &eq_idx, name_ptr, true) == ERROR)
		return (ERROR);
	*value_ptr = ft_strdup(assign + eq_idx + 1);
	if (*value_ptr == NULL)
		return (ERROR);
	return (SUCCESS);
}

t_res	init_var(t_lst **var_alst, char *envp[])
{
	int	eq_idx;

	*var_alst = NULL;
	while (*envp != NULL)
	{
		if (is_valid_var_name(*envp, &eq_idx, true))
		{
			if (push_var(var_alst, *envp, false) == ERROR)
			{
				lst_clear(var_alst);
				return (ERROR);
			}
		}
		envp += 1;
	}
	return (SUCCESS);
}
