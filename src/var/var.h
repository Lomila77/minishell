/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   var.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/08 14:03:57 by huthiess          #+#    #+#             */
/*   Updated: 2022/03/08 14:03:57 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VAR_H
# define VAR_H

# include "../data.h"

char	*ft_strncpy(char *dest, const char *src, size_t n);
char	*ft_strndup(const char *str, size_t n);
t_res	parse_var_name(char *str, int *end, char **name_ptr, bool test_equal);
t_res	parse_var_assign(char *assign, char **name_ptr, char **value_ptr);
t_res	init_var(t_lst **var_alst, char *envp[]);

#endif
