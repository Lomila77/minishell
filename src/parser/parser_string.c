/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_string.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/19 11:00:59 by huthiess          #+#    #+#             */
/*   Updated: 2022/05/19 11:00:59 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../data.h"
#include "../error/error.h"
#include "../../libft/libft.h"
#include "parser.h"

#include <stdbool.h>
#include <stdlib.h>

t_res	join_str(char **str_ptr, char *str_to_append)
{
	size_t	len1;
	size_t	len2;
	char	*new_str;

	if (*str_ptr == NULL)
		*str_ptr = ft_strdup(str_to_append);
	else
	{
		len1 = ft_strlen(*str_ptr);
		len2 = ft_strlen(str_to_append);
		new_str = malloc(sizeof(char) * (len1 + len2 + 1));
		if (new_str == NULL)
			return (ERROR);
		ft_memcpy(new_str, *str_ptr, len1);
		ft_memcpy(new_str + len1, str_to_append, len2);
		new_str[len1 + len2] = '\0';
		free(*str_ptr);
		*str_ptr = new_str;
	}
	return (SUCCESS);
}

static t_res	tk_str_to_str_error(char **str_ptr)
{
	free(*str_ptr);
	return (error("minishell: Bad format\n"));
}

t_res	parse_string(t_lst **curr_tk_alst, char **str_ptr,
			bool should_expand_vars, t_lst *var_lst)
{
	char	*str_to_append;
	t_lst	*curr_str_lst;

	*str_ptr = NULL;
	if ((*curr_tk_alst)->content.tk.type != STRING)
		return (error("minishell: Bad format\n"));
	curr_str_lst = (*curr_tk_alst)->content.tk.content.m_str_lst;
	while (curr_str_lst != NULL)
	{
		str_to_append = curr_str_lst->content.tk.content.m_str;
		if (should_expand_vars
			&& curr_str_lst->content.tk.type != SMP_QUOTED_STR
			&& variable_expansion(curr_tk_alst, curr_str_lst, &str_to_append,
				var_lst) == ERROR)
			return (tk_str_to_str_error(str_ptr));
		if (join_str(str_ptr, str_to_append) == ERROR)
			return (tk_str_to_str_error(str_ptr));
		if (str_to_append != curr_str_lst->content.tk.content.m_str)
			free(str_to_append);
		curr_str_lst = curr_str_lst->m_next;
	}
	*curr_tk_alst = (*curr_tk_alst)->m_next;
	return (SUCCESS);
}
