/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_var_exp.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/19 11:07:20 by huthiess          #+#    #+#             */
/*   Updated: 2022/05/19 11:07:20 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../data.h"
#include "../../libft/libft.h"
#include "../var/var.h"
#include "../token/tokenizer.h"
#include "../error/error.h"
#include "parser.h"

void	lst_add_after(t_lst **lst_ptr, t_lst *new)
{
	if (*lst_ptr == NULL)
		*lst_ptr = new;
	else
	{
		new->m_next = (*lst_ptr)->m_next;
		(*lst_ptr)->m_next = new;
	}
}

void	skip_to_whitespace(int *idx_ptr, char *str)
{
	while (str[*idx_ptr] != '\0'
		&& is_whitespace(str[*idx_ptr]) == NO)
		*idx_ptr += 1;
}

t_res	t_error(char *new_str)
{
	free(new_str);
	return (error(NULL));
}

t_res	t(int *i, char **str_to_append, t_lst **lst_append, t_lst *curr_str_lst)
{
	int		start;
	char	*new_str;
	t_lst	*new_str_alst;

	while ((*str_to_append)[*i] != '\0')
	{
		while (is_whitespace((*str_to_append)[*i]) == YES)
			*i += 1;
		start = *i;
		skip_to_whitespace(i, *str_to_append);
		new_str = ft_strndup((*str_to_append) + start, *i - start);
		if (new_str == NULL)
			return (t_error(new_str));
		if (new_str_lst(&new_str_alst) == ERROR)
			return (t_error(new_str));
		lst_add_after(lst_append, new_str_alst);
		lst_append = &(*lst_append)->m_next;
		if (push_str(new_str_alst, SMP_QUOTED_STR, new_str) == ERROR)
			return (t_error(new_str));
	}
	lst_last(new_str_alst->content.tk.content.m_str_lst)->m_next
	= curr_str_lst->m_next;
	curr_str_lst->m_next = NULL;
	free(*str_to_append);
	return (SUCCESS);
}

t_res	variable_expansion(t_lst **curr_tk_alst, t_lst *curr_str_lst,
			char **str_to_append, t_lst *var_lst)
{
	int		i;
	char	*new_str_to_append;
	t_lst	**lst_append;

	if (expand_vars_from_string(var_lst, str_to_append) == ERROR)
		return (ERROR);
	if (curr_str_lst->content.tk.type == NON_QUOTED_STR)
	{
		i = 0;
		skip_to_whitespace(&i, *str_to_append);
		if ((*str_to_append)[i] != '\0')
		{
			new_str_to_append = ft_strndup(*str_to_append, i);
			if (new_str_to_append == NULL)
				return (error(NULL));
			lst_append = curr_tk_alst;
			if (t(&i, str_to_append, lst_append, curr_str_lst) == ERROR)
			{
				free(new_str_to_append);
				return (ERROR);
			}
			*str_to_append = new_str_to_append;
		}
	}
	return (SUCCESS);
}
