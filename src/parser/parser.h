/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/18 10:40:34 by huthiess          #+#    #+#             */
/*   Updated: 2022/05/18 10:40:34 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSER_H
# define PARSER_H

# include "../data.h"

t_res	expand_vars_from_string(t_lst *var_lst, char **str_ptr);
t_res	variable_expansion(t_lst **curr_tk_alst, t_lst *curr_str_lst,
			char **str_to_append, t_lst *var_lst);
t_res	parse_string(t_lst **curr_tk_alst, char **str_ptr,
			bool should_expand_vars, t_lst *var_lst);

#endif
