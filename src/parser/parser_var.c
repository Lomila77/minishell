/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_var.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/18 10:38:12 by huthiess          #+#    #+#             */
/*   Updated: 2022/05/18 10:38:12 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../data.h"
#include "../../libft/libft.h"
#include "../var/var.h"

#include <stdlib.h>

t_res	get_value(t_lst *var_lst, char *start, int *name_len, char **value)
{
	char	*name;

	if (*start == '?')
	{
		*name_len = 1;
		*value = ft_itoa(g_minishell.last_return);
		if (*value == NULL)
			return (ERROR);
	}
	else
	{
		if (parse_var_name(start, name_len, &name, false) == ERROR)
			return (ERROR);
		*value = get_var(var_lst, name);
		free(name);
	}
	return (SUCCESS);
}

t_res	expand_var(t_lst *var_lst, char **str_ptr, size_t *i, char **str_tmp)
{
	int		name_len;
	char	*value;
	int		value_len;
	int		rest_len;

	if (get_value(var_lst, *str_ptr + *i + 1, &name_len, &value) == ERROR)
		return (ERROR);
	value_len = ft_strlen(value);
	rest_len = ft_strlen(*str_ptr + *i + name_len);
	*str_tmp = malloc(sizeof(char) * (*i + value_len + rest_len + 1));
	if (*str_tmp == NULL)
		return (ERROR);
	ft_memcpy(*str_tmp, *str_ptr, *i);
	ft_memcpy(*str_tmp + *i, value, value_len);
	if (*(*str_ptr + *i + 1) == '?')
		ft_free(&value);
	ft_memcpy(*str_tmp + *i + value_len, *str_ptr + *i + name_len + 1,
		rest_len);
	(*str_tmp)[*i + value_len + rest_len] = '\0';
	*i += value_len;
	return (SUCCESS);
}

t_res	expand_vars_from_string(t_lst *var_lst, char **str_ptr)
{
	size_t	i;
	bool	first;
	char	*str_tmp;

	first = true;
	i = 0;
	while ((*str_ptr)[i] != '\0')
	{
		if ((*str_ptr)[i] == '$')
		{
			if (expand_var(var_lst, str_ptr, &i, &str_tmp) == ERROR)
			{
				if (!first)
					free(*str_ptr);
				return (ERROR);
			}
			if (!first)
				free(*str_ptr);
			first = false;
			*str_ptr = str_tmp;
		}
		else
			i += 1;
	}
	return (SUCCESS);
}
