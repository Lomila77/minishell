/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/28 15:28:50 by huthiess          #+#    #+#             */
/*   Updated: 2022/02/28 15:28:50 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../data.h"
#include "../../libft/libft.h"
#include "../error/error.h"
#include "parser.h"

#include <stdbool.h>
#include <stdlib.h>

t_res	parse_exec(t_lst **curr_tk_alst, t_lst *cmd, t_lst *var_lst)
{
	char	*exec;

	if (!(*curr_tk_alst != NULL
			&& (*curr_tk_alst)->content.tk.type == STRING))
		return (error("minishell: Bad format\n"));
	if (parse_string(curr_tk_alst, &exec, true, var_lst) == ERROR)
		return (ERROR);
	if (push_arg(&cmd->content.cmd.m_args_lst, exec) == ERROR)
		return (ERROR);
	return (SUCCESS);
}

t_res	parse_arg(t_lst **curr_tk_alst, t_lst *cmd, t_lst *var_lst)
{
	char	*arg;

	if (*curr_tk_alst != NULL
		&& (*curr_tk_alst)->type == TK
		&& (*curr_tk_alst)->content.tk.type == STRING)
	{
		if (parse_string(curr_tk_alst, &arg, true, var_lst) == ERROR)
			return (ERROR);
		if (push_arg(&cmd->content.cmd.m_args_lst, arg) == ERROR)
		{
			free(arg);
			return (ERROR);
		}
	}
	return (SUCCESS);
}

t_res	parse_redir(t_lst **curr_tk_alst, t_lst *cmd, t_lst *var_lst)
{
	char			*m_file_delim;
	t_redir_type	redir_type;

	if (*curr_tk_alst != NULL && (*curr_tk_alst)->type == TK
		&& ((*curr_tk_alst)->content.tk.type == CHAR_REDIR_IN
			|| (*curr_tk_alst)->content.tk.type == CHAR_REDIR_OUT
			|| (*curr_tk_alst)->content.tk.type == CHAR_REDIR_APPEND
			|| (*curr_tk_alst)->content.tk.type == CHAR_REDIR_HEREDOC))
	{
		redir_type = (t_redir_type)(*curr_tk_alst)->content.tk.type;
		*curr_tk_alst = (*curr_tk_alst)->m_next;
		if (*curr_tk_alst == NULL)
			return (error("minishell: Bad format\n"));
		if (parse_string(curr_tk_alst, &m_file_delim,
				redir_type == REDIR_HEREDOC, var_lst) == ERROR)
			return (ERROR);
		if (push_redir(&cmd->content.cmd.m_redir_lst,
				redir_type, m_file_delim) == ERROR)
		{
			free(m_file_delim);
			return (ERROR);
		}
	}
	return (SUCCESS);
}

t_res	parse_cmd(t_lst **curr_tk_alst, t_lst **cmd_alst, t_lst *var_lst)
{
	t_lst	*cmd;

	if (!(*curr_tk_alst != NULL && new_cmd(&cmd) != ERROR))
		return (ERROR);
	lst_add_back(cmd_alst, cmd);
	while (*curr_tk_alst != NULL && (*curr_tk_alst)->type == TK
		&& ((*curr_tk_alst)->content.tk.type == CHAR_REDIR_IN
			|| (*curr_tk_alst)->content.tk.type == CHAR_REDIR_OUT
			|| (*curr_tk_alst)->content.tk.type == CHAR_REDIR_APPEND
			|| (*curr_tk_alst)->content.tk.type == CHAR_REDIR_HEREDOC))
	{
		if (parse_redir(curr_tk_alst, cmd, var_lst) == ERROR)
			return (ERROR);
	}
	if (parse_exec(curr_tk_alst, cmd, var_lst) == ERROR)
		return (ERROR);
	while (*curr_tk_alst != NULL
		&& (*curr_tk_alst)->content.tk.type != CHAR_PIPE)
	{
		if (parse_arg(curr_tk_alst, cmd, var_lst) == ERROR)
			return (ERROR);
		if (parse_redir(curr_tk_alst, cmd, var_lst) == ERROR)
			return (ERROR);
	}
	return (SUCCESS);
}

void	parse(t_lst *tk_lst, t_lst **cmd_alst, t_lst *var_lst)
{
	t_lst	*curr_tk_lst;

	curr_tk_lst = tk_lst;
	if (curr_tk_lst == NULL)
		return ;
	if (parse_cmd(&curr_tk_lst, cmd_alst, var_lst) == ERROR)
	{
		lst_clear(cmd_alst);
		return ;
	}
	while (curr_tk_lst != NULL)
	{
		if (!(curr_tk_lst->type == TK
				&& curr_tk_lst->content.tk.type == CHAR_PIPE))
		{
			lst_clear(cmd_alst);
			return ;
		}
		curr_tk_lst = curr_tk_lst->m_next;
		if (parse_cmd(&curr_tk_lst, cmd_alst, var_lst) == ERROR)
		{
			lst_clear(cmd_alst);
			return ;
		}
	}
}
