NAME	=	minishell
SRC		=	src/main.c \
			src/parser/parser.c \
			src/parser/parser_var.c \
			src/parser/parser_string.c \
			src/parser/parser_var_exp.c \
			src/error/error.c \
			src/var/init_var.c \
			src/lst/lst_push_new.c \
			src/lst/lst_push_new_bis.c \
			src/lst/lst_new.c \
			src/lst/lst_new_bis.c \
			src/lst/lst_delone.c \
			src/lst/lst_clear.c \
			src/lst/lst_add_front.c \
			src/lst/lst_add_back.c \
			src/lst/lst_last.c \
			src/lst/lst_size.c \
			src/lst/lst_var.c \
			src/token/tools_token.c \
			src/token/tokenizer.c \
			src/token/tokenizer_bis.c \
			src/prexec/heredoc.c \
			src/prexec/pipe.c \
			src/prexec/fork.c \
			src/prexec/redir.c \
			src/prexec/prexec_tools.c \
			src/prexec/exec.c \
			src/prexec/builtins.c \
			src/prexec/builtins_env.c \
			src/free.c \
			src/signal.c \
			src/readline_utils.c
INCLUDE	=	-I src/
OBJ		=	$(SRC:.c=.o)
CC		=	gcc
LIBFT	=	libft/libft.a
CFLAGS	=	-Wall -Wextra -Werror -g
LDFLAGS	=	-lreadline

all		:	${NAME}
.PHONY	:	all

%.o		:	%.c
			${CC} ${CFLAGS} -c $< -o $@ ${INCLUDE}

${NAME}	:	${OBJ}
			make -C libft -f Makefile
			${CC} ${CFLAGS} ${INCLUDE} ${OBJ} ${LIBFT} ${LDFLAGS} -o ${NAME}

clean	:
			rm -f ${OBJ}
			make fclean -C libft -f Makefile
.PHONY	:	clean

fclean	:	clean
			rm -f ${NAME}
.PHONY	:	fclean

re		:	fclean all
.PHONY	:	re
