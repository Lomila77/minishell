/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   first_and_last_is_char.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/25 11:24:38 by gcolomer          #+#    #+#             */
/*   Updated: 2021/11/25 11:24:38 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	first_and_last_is_char(const char *str, char c)
{
	if (str[0] != c || str[ft_strlen(str) - 1] != c)
		return (NO);
	return (YES);
}
