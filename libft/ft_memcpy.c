/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/16 15:11:44 by gcolomer          #+#    #+#             */
/*   Updated: 2020/11/21 20:07:35 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	unsigned long int	i;
	unsigned char		*c;
	unsigned char		*d;

	i = 0;
	c = (unsigned char *)dst;
	d = (unsigned char *)src;
	if (c == d)
		return (dst);
	while (i < n)
	{
		c[i] = d[i];
		i++;
	}
	return (dst);
}
