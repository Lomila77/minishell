/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/19 08:48:19 by gcolomer          #+#    #+#             */
/*   Updated: 2020/11/20 16:32:04 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char	*bad_start(char *str)
{
	str = ft_calloc(1, sizeof(char));
	if (str == ERROR_N)
		return (ERROR_N);
	return (str);
}

char	*ft_substr(char *src, unsigned int start, size_t len)
{
	char	*str;
	size_t	i;

	i = 0;
	str = NULL;
	if (start >= ft_strlen(src))
		return (bad_start(str));
	if (ft_strlen(src) < len)
		ft_strlen(&src[start]);
	str = ft_calloc(len + 1, sizeof(char));
	if (str == ERROR_N)
		return (ERROR_N);
	while (i < len && src[start])
	{
		str[i] = src[start];
		start++;
		i++;
	}
	return (str);
}
