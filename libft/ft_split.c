/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/04 11:16:43 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/05 10:12:05 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	ft_sepast(char *s, char c)
{
	size_t	i;

	i = 0;
	while (s[i] == c)
	{
		i++;
	}
	return (i);
}

static size_t	ft_nword(char *s, char c)
{
	size_t	n;
	size_t	i;

	n = 0;
	i = 0;
	i = ft_sepast(s, c);
	while (i < ft_strlen(s) && s[i])
	{
		i += ft_sepast(&s[i], c);
		if (s[i] && s[i] != c)
		{
			n++;
			while (s[i] && s[i] != c)
				i++;
		}
	}
	return (n);
}

static size_t	ft_strlenbsep(char *s, char c)
{
	size_t	len;
	int		i;

	len = 0;
	i = ft_sepast(s, c);
	while (s[i] && s[i] != c)
	{
		len++;
		i++;
	}
	return (len);
}

static void	*ft_nfree(char **str, ssize_t j)
{
	while (--j >= 0)
		free(str[j]);
	free(str);
	return (NULL);
}

char	**ft_split(char *src, char c)
{
	char	**str;
	ssize_t	j;
	ssize_t	i;
	ssize_t	stop;

	j = 0;
	i = 0;
	stop = ft_nword(src, c);
	str = ft_calloc(stop + 1, sizeof(char *));
	if (str == ERROR_N)
		return (str);
	while (stop)
	{
		i += ft_sepast(&src[i], c);
		str[j] = ft_calloc((ft_strlenbsep(&src[i], c) + 1), sizeof(char));
		if (str[j] == ERROR_N)
			return (ft_nfree(str, j));
		ft_strlcpy(str[j], &src[i], ft_strlenbsep(&src[i], c) + 1);
		i += ft_strlenbsep(&src[i], c) + 1;
		j++;
		stop--;
	}
	return (str);
}
