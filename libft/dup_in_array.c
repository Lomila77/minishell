/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   int_has_dup.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/06 14:04:25 by gcolomer          #+#    #+#             */
/*   Updated: 2021/09/06 14:04:25 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	dup_in_array(int *array, int len, int indice[2])
{
	int	i;
	int	j;

	i = 0;
	while (i < len)
	{
		j = i + 1;
		while (j < len)
		{
			if (array[i] == array[j])
			{
				indice[0] = i;
				indice[1] = j;
				return (YES);
			}
			j++;
		}
		i++;
	}
	return (NO);
}
