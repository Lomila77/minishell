/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/18 15:44:59 by gcolomer          #+#    #+#             */
/*   Updated: 2020/11/20 21:02:51 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	size_t	i;
	size_t	a;
	size_t	b;
	char	*to_find;
	char	*str;

	i = -1;
	a = 0;
	to_find = (char *)needle;
	str = (char *)haystack;
	if (!(to_find[0]))
		return (str);
	while (str[++i] && i < len)
	{
		b = i;
		while (str[b] == to_find[a] && to_find[a] && b < len)
		{
			a++;
			b++;
		}
		if (!(to_find[a]))
			return (&str[i]);
		a = 0;
	}
	return (NULL);
}
