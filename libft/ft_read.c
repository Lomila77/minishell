/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/24 14:05:17 by gcolomer          #+#    #+#             */
/*   Updated: 2021/11/24 14:05:17 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <unistd.h>

int	ft_read(int fd, char buf[SIZE_B], int *ret)
{
	*ret = read(fd, buf, SIZE_B);
	if (*ret == -1)
		return (ERROR);
	else if (*ret == 0)
		return (EOF_);
	return (SUCCESS);
}
