/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   array_is_sort.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/19 18:38:16 by gcolomer          #+#    #+#             */
/*   Updated: 2021/11/19 18:38:16 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	array_is_sort(int *array, int len)
{
	int	top;
	int	bot;

	top = 0;
	bot = top + 1;
	while (bot < len)
	{
		if (array[bot] < array[top])
			return (NO);
		top++;
		bot = top + 1;
	}
	return (SUCCESS);
}
