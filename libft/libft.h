/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/14 17:33:15 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/05 10:29:13 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <unistd.h>
# include <stdlib.h>

# define SUCCESS 1
# define ERROR -1
# define ERROR_N NULL

# define EOF_ 0

# define STDIN 0
# define STDOUT 1
# define STDERR 2

# define NO -2
# define YES 1

# define CONTINUE 1

# define WHITE 0xffffff

# define SIZE_B 6

# define OLD 0
# define NEW 1

# define TRUE 0
# define FALSE 1

typedef struct s_cursor
{
	int	matrice;
	int	string;
}	t_cursor;

typedef struct s_list
{
	void			*content;
	struct s_list	*next;
}	t_list;

/* Memory handler */
void				*ft_memset(void *b, int c, size_t len);
void				*ft_memcpy(void *dst, const void *src, size_t n);
void				*ft_memccpy(void *dest, const void *src, int c, size_t n);
void				*ft_memmove(void *dest, const void *src, size_t len);
void				*ft_memchr(const void *s, int c, size_t n);
int					ft_memcmp(const void *s1, const void *s2, size_t n);

/* Matrice Handler */
int					len_matrice(char **matrice);
int					empty_in_matrice(char **matrice);
void				free_matrice(char **matrice);
int					v_char_is_in_matrice(char **matrice, char c);
int					only_char_in_matrice(char *char_of_matrice, char **matrice);

/* Int handler */
void				swap_int(int *a, int *b);

/* Init to NULL */
void				ft_bzero(void *s, size_t n);
void				*ft_calloc(size_t count, size_t size);
int					ft_free(char **to_free);
char				*free_ret_null(char **str);
void				be_null4(char **s1, char **s2, char **s3, char **s4);
void				be_null2(char **s1, char **s2);
void				init_array_to_0(int	*array, int len);
int					mult_free_err(char *f1, char *f2, char *f3, char *f4);

/* Check str */
int					str_have_space(char *str);
size_t				ft_strlen(const char *str);
int					ft_count(char c, char *str);
int					first_and_last_is_char(const char *str, char c);

/* Check array */
int					dup_in_array(int *array, int len, int indice[2]);
int					array_is_sort(int *array, int len);

/* What is char > */
int					ft_isalpha(int c);
int					ft_isdigit(int c);
int					ft_isalnum(int c);
int					ft_isascii(int c);
int					ft_isprint(int c);
int					ft_isspace(char c);
int					ft_isnum(int c);
int					ft_space(char c);
int					ft_isn(char c);

/* What is str */
int					ft_str_isnegnum(unsigned char *str);
int					str_have_c(char *str, char c);
int					count_char(char *str, char c);
int					str_is_num(char *str);

/* Str handler */
int					ft_strncmp(char *s1, char *s2, size_t n);
size_t				ft_strlcat(char *dest, char *src, size_t size);
char				*ft_strchr(const char *s, int c);
size_t				ft_strlcpy(char *dest, const char *src, size_t dstsize);
char				*ft_strnstr(const char *haystack, const char *needle,
						size_t len);
char				*ft_strrchr(const char *s, int c);
char				*ft_substr(char *str, unsigned int start, size_t len);
char				*ft_strjoin(char const *s1, char const *s2);
char				**ft_split(char *src, char c);
char				*ft_strdup(char *src);
char				*ft_strcdup(const char *str, char c);
char				*ft_strmapi(char const *s, char (*f)(unsigned int, char));
char				*ft_strtrim(char const *s1, char const *set);
size_t				ft_nstrchr(const char *s, int c);
size_t				ft_nnstrchr(const char *s, int c);
char				*ft_strccpy(char *str, char c);
char				*ft_strjoin_sep(char *s1, char *s2, char c);
char				*ft_strlnew(char **str, char del, int free);
int					str_is_identical(char *str1, char *str2);
char				*ft_strncpy(char *dest, const char *src, size_t n);

/* Char and Integer */
int					ft_atoi(const char *str, int *nb);
char				*ft_itoa(int n);
int					ft_batoi(const char *str, int length);
unsigned char		*ft_sitoa(long long int n);
unsigned char		*ft_uitoa(unsigned long long int n);
unsigned long long	ft_atoiu(const char *str);
double				ft_atod(const char *str);

/* Modify char */
int					ft_tolower(int c);
int					ft_toupper(int c);

/* Writable func */
void				ft_putchar_fd(char c, int ft);
void				ft_putstr(char *str);
void				ft_putstr_fd(const char *s, int fd);
void				ft_putendl_fd(char *s, int fd);
void				ft_putnbr_fd(int n, int fd);

/* Readable func */
int					n_read(int fd, char **buf, int len, int *ret);
int					ft_read(int fd, char buf[SIZE_B], int *ret);
char				*read_line(int fd, char stop);

/* List func */
t_list				*ft_lstnew(void *content);
void				ft_lstadd_front(t_list **alst, t_list *new);
int					ft_lstsize(t_list *lst);
t_list				*ft_lstlast(t_list *lst);
void				ft_lstadd_back(t_list **alst, t_list *new);
void				ft_lstdelone(t_list *lst, void (*del)(void *));
void				ft_lstclear(t_list **lst, void (*del)(void *));
void				ft_lstiter(t_list *lst, void (*f)(void *));
t_list				*ft_lstmap(t_list *lst, void *(*f)(void *),
						void (*del)(void *));

#endif
