/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/14 17:56:31 by gcolomer          #+#    #+#             */
/*   Updated: 2020/12/14 17:56:56 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static unsigned long	ft_wneg(int n, char *str)
{
	unsigned long		nbr;

	if (n < 0)
	{
		nbr = (long)n * -1;
		str[0] = '-';
	}
	else
		nbr = n;
	return (nbr);
}

static unsigned long	ft_neg(int n, int *val)
{
	unsigned long		nbr;

	if (n < 0)
	{
		*val = 1;
		nbr = (long)n * -1;
	}
	else
		nbr = n;
	return (nbr);
}

char	*ft_itoa(int n)
{
	unsigned long		nbr;
	int					len;
	char				*str;

	len = 0;
	nbr = ft_neg(n, &len);
	while (nbr >= 10)
	{
		nbr /= 10;
		len++;
	}
	len++;
	str = malloc(sizeof(char) * len + 1);
	if (str == ERROR_N)
		return (ERROR_N);
	nbr = ft_wneg(n, str);
	str[len] = '\0';
	while (nbr >= 10)
	{
		str[--len] = nbr % 10 + 48;
		nbr /= 10;
	}
	str[--len] = nbr + 48;
	return (str);
}
