/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcdup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/03 16:18:52 by huthiess          #+#    #+#             */
/*   Updated: 2022/04/03 16:18:52 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

#include <stdlib.h>

char	*ft_strncpy(char *dest, const char *src, size_t n)
{
	char	*end;

	end = ft_memccpy(dest, src, '\0', n - 1);
	if (end == NULL)
		dest[n - 1] = '\0';
	else
		ft_bzero(end, n - (end - dest));
	return (dest);
}

char	*ft_strcdup(const char *str, char c)
{
	size_t	size;
	char	*new;
	void	*end;

	size = ft_strlen(str);
	end = ft_memchr(str, c, size);
	if (end != NULL)
		size = end - (void *)str;
	new = (char *)malloc(sizeof(*str) * (size + 1));
	if (new == NULL)
		return (NULL);
	return (ft_strncpy(new, str, size + 1));
}
