/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/27 18:24:33 by gcolomer          #+#    #+#             */
/*   Updated: 2021/07/27 18:24:33 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strccpy(char *str, char c)
{
	char	*ret;
	int		i;

	i = 0;
	while (str[i] != c && str[i])
		i++;
	ret = malloc(sizeof(char) * (i + 1));
	if (ret == ERROR_N)
		return (ERROR_N);
	ft_strlcpy(ret, (const char *)str, i + 1);
	return (ret);
}
