/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   only_char_in_matrice.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/31 10:31:36 by gcolomer          #+#    #+#             */
/*   Updated: 2022/01/31 10:31:36 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	only_char_in_matrice(char *char_of_matrice, char **matrice)
{
	int	i;
	int	j;
	int	k;

	i = 0;
	j = 0;
	k = 0;
	while (matrice[i])
	{
		j = 0;
		while (matrice[i][j])
		{
			k = 0;
			while (char_of_matrice[k])
			{
				if (char_of_matrice[k] == matrice[i][j])
					return (NO);
				k++;
			}
			j++;
		}
		i++;
	}
	return (YES);
}
