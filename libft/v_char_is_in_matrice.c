/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   v_char_is_in_matrice.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/25 11:42:02 by gcolomer          #+#    #+#             */
/*   Updated: 2021/11/25 11:42:02 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	v_char_is_in_matrice(char **matrice, char c)
{
	t_cursor	i;
	int			find;

	i.matrice = 0;
	find = 0;
	while (matrice[i.matrice] != NULL)
	{
		i.string = 0;
		while (matrice[i.matrice][i.string])
		{
			if (matrice[i.matrice][i.string] == c)
				find++;
			i.string++;
		}
		i.matrice++;
	}
	if (find == 0)
		return (NO);
	return (find);
}
