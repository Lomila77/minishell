/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/18 17:14:06 by gcolomer          #+#    #+#             */
/*   Updated: 2020/11/22 15:15:23 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

#include <stdio.h>

void	set_to_zero(int *i, int *val, long long *res)
{
	*i = 0;
	*val = 0;
	*res = 0;
}

int	ft_atoi(const char *str, int *nb)
{
	int				i;
	int				val;
	long long		res;

	set_to_zero(&i, &val, &res);
	while (ft_isspace(str[i]) == YES)
		i++;
	if (str[i] == '-')
	{
		val = 1;
		i++;
	}
	else if (str[i] == '+')
		i++;
	while (str[i] >= '0' && str[i] <= '9' && res <= 2147483647)
	{
		res = res * 10 + (str[i] - 48);
		i++;
	}
	if (val % 2 != 0)
		res *= -1;
	if (res > 2147483647 || res < -2147483648)
		return (ERROR);
	*nb = res;
	return (SUCCESS);
}
