/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_line.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/23 21:58:36 by gcolomer          #+#    #+#             */
/*   Updated: 2021/11/23 21:58:36 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	read_stop(char **str, char **line, char stop, char buffer[SIZE_B])
{
	char	tmp[SIZE_B + 1];
	int		indice_stop;

	indice_stop = ft_count(stop, buffer);
	if (str_have_c(buffer, stop) == YES)
	{
		*line = ft_strlnew(str, stop, NO);
		ft_free(str);
		ft_strlcpy(tmp, &buffer[indice_stop + 1], SIZE_B - indice_stop);
		ft_bzero((void *)buffer, SIZE_B + 1);
		ft_strlcpy(buffer, tmp, SIZE_B - indice_stop);
		return (YES);
	}
	return (NO);
}

char	*read_file(char stop, char **tmp)
{
	char	*line;

	line = NULL;
	if (stop == '\0')
		line = ft_strlnew(tmp, stop, NO);
	ft_free(tmp);
	return (line);
}

int	ret_read(int readed, char **line)
{
	if (readed == ERROR)
		ft_free(line);
	if (readed == 0)
		return (YES);
	return (NO);
}

//char	*read_line(int fd, char stop)
//{
//	static char	buffer[SIZE_B + 1];
//	char		*tmp[2];
//	char		*line;
//	int			readed;
//
//	readed = 0;
//	ft_bzero((void *)&buffer, SIZE_B + 1);
//	be_null2(&tmp[NEW], &tmp[OLD]);
//	tmp[OLD] = ft_strdup(buffer);
//	if (read_stop(&tmp[OLD], &line, stop, buffer) == YES)
//		return (line);
//	while (ft_read(fd, buffer, &readed) > 0)
//	{
//		tmp[NEW] = ft_strjoin(tmp[OLD], (const char *)&buffer);
//		ft_bzero((void *)buffer, SIZE_B);
//		ft_free(&tmp[OLD]);
//		if (read_stop(&tmp[NEW], &line, stop, buffer) == YES)
//			break ;
//		tmp[OLD] = tmp[NEW];
//	}
//	if (tmp[OLD] != tmp[NEW])
//		ft_free(&tmp[OLD]);
//	if (ret_read(readed, &line) == YES)
//		return (read_file(stop, &tmp[NEW]));
//	ft_free(&tmp[NEW]);
//	return (line);
//}
