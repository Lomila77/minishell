/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/18 14:33:06 by gcolomer          #+#    #+#             */
/*   Updated: 2020/11/18 15:01:00 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	int		i;
	char	ch;
	char	*str;

	ch = c;
	str = (char *)s;
	i = ft_strlen(str) + 1;
	while (--i >= 0)
		if (ch == str[i])
			return (&str[i]);
	if (ch == '\0')
		return (&str[ft_strlen(str)]);
	return (NULL);
}
